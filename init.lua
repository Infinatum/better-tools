-- better tools mod by Semmett9 --

-- override --

-- pick

minetest.register_tool(":default:pick_wood", {
	description = "Wooden Pickaxe",
	inventory_image = "default_tool_woodpick.png",
	tool_capabilities = {
		full_punch_interval = 1.2,
		max_drop_level=0,
		groupcaps={
			cracky = {times={[2]=2.00, [3]=1.20}, uses=10, maxlevel=1},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=5, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=10, maxlevel=1},
			snappy = {times={[3]=0.40}, uses=5, maxlevel=1},
			fleshy = {times={[2]=0.95, [3]=0.6}, uses=15, maxlevel=1}
		}
	},
})
minetest.register_tool(":default:pick_stone", {
	description = "Stone Pickaxe",
	inventory_image = "default_tool_stonepick.png",
	tool_capabilities = {
		full_punch_interval = 1.3,
		max_drop_level=0,
		groupcaps={
			cracky = {times={[1]=3.00, [2]=1.20, [3]=0.80}, uses=20, maxlevel=1},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=10, maxlevel=1},
			snappy = {times={[3]=0.40}, uses=10, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=10, maxlevel=1},
			fleshy = {times={[2]=0.7, [3]=0.5}, uses=25, maxlevel=1}
		}
	},
})
minetest.register_tool(":default:pick_steel", {
	description = "Steel Pickaxe",
	inventory_image = "default_tool_steelpick.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			cracky = {times={[1]=4.00, [2]=1.60, [3]=1.00}, uses=30, maxlevel=2},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=15, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=10, maxlevel=1},
			snappy = {times={[3]=0.40}, uses=15, maxlevel=1},
			fleshy = {times={[2]=0.6, [3]=0.35}, uses=35, maxlevel=1}
		}
	},
})

minetest.register_tool(":default:pick_bronze", {
	description = "Bronze Pickaxe",
	inventory_image = "default_tool_bronzepick.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			cracky = {times={[1]=4.00, [2]=1.60, [3]=0.80}, uses=30, maxlevel=2},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=15, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=10, maxlevel=1},
			snappy = {times={[3]=0.40}, uses=15, maxlevel=1},
			fleshy = {times={[2]=0.6, [3]=0.35}, uses=35, maxlevel=1}
		},
		damage_groups = {fleshy=4},
	},
})

minetest.register_tool(":default:pick_mese", {
	description = "Mese Pickaxe",
	inventory_image = "default_tool_mesepick.png",
	tool_capabilities = {
		full_punch_interval = 0.65,
		max_drop_level=3,
		groupcaps={
			cracky = {times={[1]=2.0, [2]=1.0, [3]=0.5}, uses=20, maxlevel=3},
			crumbly = {times={[1]=1.50, [2]=0.70, [3]=0.60}, uses=20, maxlevel=2},
			choppy={times={[1]=2.60, [2]=1.00, [3]=0.60}, uses=20, maxlevel=2},
			fleshy = {times={[2]=0.6, [3]=0.35}, uses=35, maxlevel=1}
		}
	},
})

minetest.register_tool(":default:pick_diamond", {
	description = "Diamond Pickaxe",
	inventory_image = "default_tool_diamondpick.png",
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level=3,
		groupcaps={
			cracky = {times={[1]=2.0, [2]=1.0, [3]=0.50}, uses=30, maxlevel=3},
			crumbly = {times={[1]=2.0, [2]=1.0, [3]=0.5}, uses=20, maxlevel=3},
			snappy = {times={[1]=2.0, [2]=1.0, [3]=0.5}, uses=20, maxlevel=3},
			fleshy = {times={[2]=0.6, [3]=0.5}, uses=80, maxlevel=1}
		},
		damage_groups = {fleshy=5},
	},
})

-- shovel --

minetest.register_tool(":default:shovel_wood", {
	description = "Wooden Shovel",
	inventory_image = "default_tool_woodshovel.png",
	wield_image = "default_tool_woodshovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.2,
		max_drop_level=0,
		groupcaps={
			crumbly = {times={[1]=3.00, [2]=0.80, [3]=0.50}, uses=10, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=5, maxlevel=1},
			fleshy = {times={[2]=1.05, [3]=0.70}, uses=25, maxlevel=1}
		}
	},
})
minetest.register_tool(":default:shovel_stone", {
	description = "Stone Shovel",
	inventory_image = "default_tool_stoneshovel.png",
	wield_image = "default_tool_stoneshovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.4,
		max_drop_level=0,
		groupcaps={
			crumbly = {times={[1]=1.50, [2]=0.50, [3]=0.30}, uses=20, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=10, maxlevel=1},
			fleshy = {times={[2]=0.75, [3]=0.50}, uses=40, maxlevel=1}
		}
	},
})
minetest.register_tool(":default:shovel_steel", {
	description = "Steel Shovel",
	inventory_image = "default_tool_steelshovel.png",
	wield_image = "default_tool_steelshovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.1,
		max_drop_level=1,
		groupcaps={
			crumbly = {times={[1]=1.50, [2]=0.70, [3]=0.60}, uses=30, maxlevel=2},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=15, maxlevel=1},
			fleshy = {times={[2]=0.45, [3]=0.30}, uses=50, maxlevel=1}
		}
	},
})

minetest.register_tool(":default:shovel_bronze", {
	description = "Bronze Shovel",
	inventory_image = "default_tool_bronzeshovel.png",
	wield_image = "default_tool_bronzeshovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.1,
		max_drop_level=1,
		groupcaps={
			crumbly = {times={[1]=1.50, [2]=0.90, [3]=0.40}, uses=40, maxlevel=2},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=15, maxlevel=1},
			fleshy = {times={[2]=0.45, [3]=0.30}, uses=50, maxlevel=1}
		},
		damage_groups = {fleshy=3},
	},
})
minetest.register_tool(":default:shovel_mese", {
	description = "Mese Shovel",
	inventory_image = "default_tool_meseshovel.png",
	wield_image = "default_tool_meseshovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=3,
		groupcaps={
			crumbly = {times={[1]=1.20, [2]=0.60, [3]=0.30}, uses=20, maxlevel=3},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=15, maxlevel=1},
			fleshy = {times={[2]=0.45, [3]=0.30}, uses=50, maxlevel=1}
		},
		damage_groups = {fleshy=4},
	},
})
minetest.register_tool(":default:shovel_diamond", {
	description = "Diamond Shovel",
	inventory_image = "default_tool_diamondshovel.png",
	wield_image = "default_tool_diamondshovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			crumbly = {times={[1]=1.10, [2]=0.50, [3]=0.30}, uses=30, maxlevel=3},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=15, maxlevel=1},
			fleshy = {times={[2]=0.45, [3]=0.30}, uses=50, maxlevel=1}
			
		},
		damage_groups = {fleshy=4},
	},
})


-- axe 

minetest.register_tool(":default:axe_wood", {
	description = "Wooden Axe",
	inventory_image = "default_tool_woodaxe.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=0,
		groupcaps={
			choppy = {times={[2]=1.60, [3]=1.20}, uses=10, maxlevel=1},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=5, maxlevel=1},
			fleshy = {times={[2]=0.70, [3]=0.50}, uses=5, maxlevel=1}
		}
	},
})
minetest.register_tool(":default:axe_stone", {
	description = "Stone Axe",
	inventory_image = "default_tool_stoneaxe.png",
	tool_capabilities = {
		full_punch_interval = 1.2,
		max_drop_level=0,
		groupcaps={
			choppy={times={[1]=3.00, [2]=1.40, [3]=1.00}, uses=20, maxlevel=1},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=10, maxlevel=1},
			fleshy={times={[2]=0.60, [3]=0.40}, uses=15, maxlevel=1}
		}
	},
})
minetest.register_tool(":default:axe_steel", {
	description = "Steel Axe",
	inventory_image = "default_tool_steelaxe.png",
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level=1,
		groupcaps={
			choppy={times={[1]=2.60, [2]=1.00, [3]=0.60}, uses=30, maxlevel=2},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=15, maxlevel=1},
			fleshy={times={[2]=0.40, [3]=0.25}, uses=25, maxlevel=1}
		}
	},
})

minetest.register_tool(":default:axe_bronze", {
	description = "Bronze Axe",
	inventory_image = "default_tool_bronzeaxe.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			choppy={times={[1]=2.50, [2]=1.40, [3]=1.00}, uses=30, maxlevel=2},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=15, maxlevel=1},
		},
		damage_groups = {fleshy=4},
	},
})
minetest.register_tool(":default:axe_mese", {
	description = "Mese Axe",
	inventory_image = "default_tool_meseaxe.png",
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level=1,
		groupcaps={
			choppy={times={[1]=2.20, [2]=1.00, [3]=0.60}, uses=20, maxlevel=3},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=15, maxlevel=1},
		},
		damage_groups = {fleshy=6},
	},
})
minetest.register_tool(":default:axe_diamond", {
	description = "Diamond Axe",
	inventory_image = "default_tool_diamondaxe.png",
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level=1,
		groupcaps={
			choppy={times={[1]=2.10, [2]=0.90, [3]=0.50}, uses=30, maxlevel=2},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=15, maxlevel=1},
		},
		damage_groups = {fleshy=7},
	},
})

-- sword 

minetest.register_tool(":default:sword_wood", {
	description = "Wooden Sword",
	inventory_image = "default_tool_woodsword.png",
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level=0,
		groupcaps={
			fleshy={times={[2]=1.10, [3]=0.60}, uses=10, maxlevel=1},
			snappy={times={[2]=0.9, [3]=0.45}, uses=10, maxlevel=1},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=5, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=5, maxlevel=1},
			
		}
	}
})
minetest.register_tool(":default:sword_stone", {
	description = "Stone Sword",
	inventory_image = "default_tool_stonesword.png",
	tool_capabilities = {
		full_punch_interval = 1.1,
		max_drop_level=0,
		groupcaps={
			fleshy={times={[2]=0.80, [3]=0.40}, uses=20, maxlevel=1},
			snappy={times={[2]=0.75, [3]=0.35}, uses=20, maxlevel=1},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=10, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=10, maxlevel=1},
		}
	}
})
minetest.register_tool(":default:sword_steel", {
	description = "Steel Sword",
	inventory_image = "default_tool_steelsword.png",
	tool_capabilities = {
		full_punch_interval = 0.8,
		max_drop_level=1,
		groupcaps={
			fleshy={times={[1]=2.00, [2]=0.80, [3]=0.40}, uses=10, maxlevel=2},
			snappy={times={[2]=0.70, [3]=0.30}, uses=40, maxlevel=1},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=10, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=10, maxlevel=1},
		}
	}
})

minetest.register_tool(":default:sword_bronze", {
	description = "Bronze Sword",
	inventory_image = "default_tool_bronzesword.png",
	tool_capabilities = {
		full_punch_interval = 0.8,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=2.5, [2]=1.20, [3]=0.35}, uses=40, maxlevel=2},
			snappy={times={[2]=0.70, [3]=0.30}, uses=40, maxlevel=1},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=10, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=10, maxlevel=1},
		},
		damage_groups = {fleshy=6},
	}
})
minetest.register_tool(":default:sword_mese", {
	description = "Mese Sword",
	inventory_image = "default_tool_mesesword.png",
	tool_capabilities = {
		full_punch_interval = 0.7,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=2.0, [2]=1.00, [3]=0.35}, uses=30, maxlevel=3},
			snappy={times={[2]=0.70, [3]=0.30}, uses=40, maxlevel=1},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=10, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=10, maxlevel=1},
		},
		damage_groups = {fleshy=7},
	}
})
minetest.register_tool(":default:sword_diamond", {
	description = "Diamond Sword",
	inventory_image = "default_tool_diamondsword.png",
	tool_capabilities = {
		full_punch_interval = 0.7,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=1.90, [2]=0.90, [3]=0.30}, uses=40, maxlevel=3},
			snappy={times={[2]=0.70, [3]=0.30}, uses=40, maxlevel=1},
			crumbly = {times={[1]=1.50,[2]=3.00, [3]=0.70}, uses=10, maxlevel=1},
			choppy = {times={[2]=3.60, [3]=1.20}, uses=10, maxlevel=1},
		},
		damage_groups = {fleshy=8},
	}
})



-- more tools --

minetest.register_tool("better_tools:paxel_wood", {
	description = "Wooden Paxel",
	inventory_image = "better_tools_woodpaxel.png",
	tool_capabilities = {
		full_punch_interval = 1.2,
		max_drop_level=0,
		groupcaps={
			cracky = {times={[2]=2.00, [3]=1.20}, uses=10, maxlevel=1},
			crumbly = {times={[1]=3.00, [2]=0.80, [3]=0.50}, uses=10, maxlevel=1},
			choppy = {times={[2]=1.60, [3]=1.20}, uses=10, maxlevel=1},
			snappy = {times={[3]=0.40}, uses=5, maxlevel=1},
			fleshy = {times={[2]=0.95, [3]=0.6}, uses=15, maxlevel=1}
		}
	},
})

minetest.register_tool("better_tools:paxel_stone", {
	description = "Stone Paxel",
	inventory_image = "better_tools_stonepaxel.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=2,
		groupcaps={
			cracky = {times={[1]=3.00, [2]=1.20, [3]=0.80}, uses=20, maxlevel=1},
			crumbly = {times={[1]=1.50, [2]=0.50, [3]=0.30}, uses=20, maxlevel=1},
			choppy={times={[1]=3.00, [2]=1.40, [3]=1.00}, uses=20, maxlevel=1},
			fleshy = {times={[2]=0.6, [3]=0.35}, uses=35, maxlevel=1}
		}
	},
})

minetest.register_tool("better_tools:paxel_steel", {
	description = "Steel Paxel",
	inventory_image = "better_tools_steelpaxel.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=2,
		groupcaps={
			cracky = {times={[1]=1.00, [2]=1.60, [3]=1.00}, uses=30, maxlevel=2},
			crumbly = {times={[1]=1.50, [2]=0.70, [3]=0.60}, uses=30, maxlevel=2},
			choppy={times={[1]=2.60, [2]=1.00, [3]=0.60}, uses=30, maxlevel=2},
			fleshy = {times={[2]=0.6, [3]=0.35}, uses=35, maxlevel=1}
		}
	},
})

minetest.register_tool("better_tools:paxel_bronze", {
	description = "Bronze paxel",
	inventory_image = "better_tool_bronzepaxel.png",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			cracky = {times={[1]=4.00, [2]=1.60, [3]=0.80}, uses=30, maxlevel=2},
			crumbly = {times={[1]=1.50, [2]=0.90, [3]=0.40}, uses=40, maxlevel=2},
			choppy={times={[1]=2.50, [2]=1.40, [3]=1.00}, uses=30, maxlevel=2},
			fleshy = {times={[2]=0.6, [3]=0.35}, uses=35, maxlevel=1}
		},
		damage_groups = {fleshy=4},
	},
})



minetest.register_tool("better_tools:paxel_mese", {
	description = "Mese Paxel",
	inventory_image = "better_tool_mesepaxel.png",
	tool_capabilities = {
		full_punch_interval = 0.65,
		max_drop_level=3,
		groupcaps={
			cracky = {times={[1]=2.0, [2]=1.0, [3]=0.5}, uses=20, maxlevel=3},
			crumbly = {times={[1]=2.0, [2]=1.0, [3]=0.5}, uses=20, maxlevel=3},
			snappy = {times={[1]=2.0, [2]=1.0, [3]=0.5}, uses=20, maxlevel=3},
			fleshy = {times={[2]=0.6, [3]=0.5}, uses=80, maxlevel=1}
		}
	},
})

minetest.register_tool("better_tools:paxel_diamond", {
	description = "Diamond Pickaxe",
	inventory_image = "better_tool_diamondpaxel.png",
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level=3,
		groupcaps={
			cracky = {times={[1]=2.0, [2]=1.0, [3]=0.50}, uses=30, maxlevel=3},
			crumbly = {times={[1]=1.10, [2]=0.50, [3]=0.30}, uses=30, maxlevel=3},
			choppy={times={[1]=2.10, [2]=0.90, [3]=0.50}, uses=30, maxlevel=2},
			fleshy = {times={[2]=0.6, [3]=0.5}, uses=80, maxlevel=1}
		},
		damage_groups = {fleshy=5},
	},
})


minetest.register_craft({
	output = 'better_tools:paxel_wood',
	recipe = {
		{'default:pick_wood', 'default:shovel_wood', 'default:axe_wood'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_wood',
	recipe = {
		{'default:axe_wood', 'default:pick_wood', 'default:shovel_wood'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_wood',
	recipe = {
		{'default:shovel_wood', 'default:axe_wood', 'default:pick_wood'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_wood',
	recipe = {
		{'default:axe_wood', 'default:shovel_wood', 'default:pick_wood'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_stone',
	recipe = {
		{'default:pick_stone', 'default:shovel_stone', 'default:axe_stone'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_stone',
	recipe = {
		{'default:axe_stone', 'default:pick_stone', 'default:shovel_stone'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_stone',
	recipe = {
		{'default:shovel_stone', 'default:axe_stone', 'default:pick_stone'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_stone',
	recipe = {
		{'default:axe_stone', 'default:shovel_stone', 'default:pick_stone'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_steel',
	recipe = {
		{'default:pick_steel', 'default:shovel_steel', 'default:axe_steel'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_steel',
	recipe = {
		{'default:axe_steel', 'default:pick_steel', 'default:shovel_steel'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_steel',
	recipe = {
		{'default:shovel_steel', 'default:axe_steel', 'default:pick_steel'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_steel',
	recipe = {
		{'default:axe_steel', 'default:shovel_steel', 'default:pick_steel'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})
--
minetest.register_craft({
	output = 'better_tools:paxel_bronze',
	recipe = {
		{'default:pick_bronze', 'default:shovel_bronze', 'default:axe_bronze'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_bronze',
	recipe = {
		{'default:axe_bronze', 'default:pick_bronze', 'default:shovel_bronze'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_bronze',
	recipe = {
		{'default:shovel_bronze', 'default:axe_bronze', 'default:pick_bronze'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_bronze',
	recipe = {
		{'default:axe_bronze', 'default:shovel_bronze', 'default:pick_bronze'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})
--

minetest.register_craft({
	output = 'better_tools:paxel_mese',
	recipe = {
		{'default:pick_mese', 'better_tools:shovel_mese', 'better_tools:axe_mese'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_mese',
	recipe = {
		{'better_tools:axe_mese', 'default:pick_mese', 'better_tools:shovel_mese'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_mese',
	recipe = {
		{'better_tools:shovel_mese', 'better_tools:axe_mese', 'default:pick_mese'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_mese',
	recipe = {
		{'better_tools:axe_mese', 'better_tools:shovel_mese', 'default:pick_mese'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_diamond',
	recipe = {
		{'default:pick_diamond', 'default:shovel_diamond', 'default:axe_diamond'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_diamond',
	recipe = {
		{'default:axe_diamond', 'default:pick_diamond', 'default:shovel_diamond'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_diamond',
	recipe = {
		{'default:shovel_diamond', 'default:axe_diamond', 'default:pick_diamond'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})

minetest.register_craft({
	output = 'better_tools:paxel_diamond',
	recipe = {
		{'default:axe_diamond', 'default:shovel_diamond', 'default:pick_diamond'},
		{'', 'default:stick', ''},
		{'', 'default:stick', ''},
	}
})


